#include<stdio.h>
int main()
{
    int row,column;
    printf("Enter the number of rows :");
    scanf("%d",&row);
    printf("Enter the number of columns :");
    scanf("%d",&column);
    int array1[row][column];
    printf("For array1 -\n");
    for(int i=0; i<=row-1; i++)
    {
        for(int j=0; j<=column-1; j++)
        {
            printf("Enter the element of row number %d and  column number %d of array 1\n",i+1,j+1);
            scanf("%d",&array1[i][j]);
        }
    }
    printf("Array1 :\n");
    for(int i=0; i<=row-1; i++)
    {
        for(int j=0; j<=column-1; j++)
        {
            printf ("%d\t",array1[i][j]);
        }
        printf("\n");
    }
    int array2[row][column];
    printf("For array2 -\n");
    for(int i=0; i<=row-1; i++)
    {
        for(int j=0; j<=column-1; j++)
        {
            printf("enter the element of row number %d and  column number %d of array 2\n",i+1,j+1);
            scanf("%d",&array2[i][j]);
        }
    }
    printf("Array2 :\n");
    for(int i=0; i<=row-1; i++)
    {
        for(int j=0; j<=column-1; j++)
        {
            printf ("%d\t",array2[i][j]);
        }
        printf("\n");
    }
    int array3[row][column];
      for(int i=0; i<=row-1; i++)
        {
            for(int j=0; j<=column-1; j++)
            {
                array3[i][j]=array1[i][j]-array2[i][j];
            }
        }
        printf("Difference of the given two arrays is :\n");
        for(int i=0; i<=row-1; i++)
        {
            for(int j=0; j<=column-1; j++)
            {
                printf ("%d\t",array3[i][j]);
            }
            printf("\n");
        }
        return 0;
	}